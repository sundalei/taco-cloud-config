package sia.data;

import org.springframework.data.repository.CrudRepository;
import sia.Taco;

public interface TacoRepository extends CrudRepository<Taco, Long> {

}
