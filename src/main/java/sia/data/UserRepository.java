package sia.data;

import org.springframework.data.repository.CrudRepository;

import sia.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	User findByUsername(String username);
}
