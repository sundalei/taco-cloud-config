package sia;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

import sia.data.IngredientRepository;
import sia.data.UserRepository;

@SpringBootApplication
public class TacoCloudConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(TacoCloudConfigApplication.class, args);
    }
    
    @Bean
    @Profile("!prod")
    public CommandLineRunner dataLoader(IngredientRepository repo, UserRepository userRepo, PasswordEncoder encoder) {
    	return args -> {
    		System.out.println("hello world");
    	};
    }
}
