package sia;

import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class IngredientAttributeConverter implements AttributeConverter<Ingredient.Type, String> {

    @Override
    public String convertToDatabaseColumn(Ingredient.Type attribute) {
        return attribute.toString();
    }

    @Override
    public Ingredient.Type convertToEntityAttribute(String dbData) {
        return Ingredient.Type.valueOf(dbData);
    }
}
